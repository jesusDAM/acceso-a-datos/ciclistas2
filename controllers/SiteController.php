<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrud(){
        return $this->render("gestion");
    }
    
    public function actionConsulta1a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) as ciclistas"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS ciclistas FROM ciclista",
        ]);
    }
    
    public function actionConsulta1(){
        $numero = Yii::$app->db
                ->createCommand('select count(*) as ciclistas from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS ciclistas FROM ciclista',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS ciclistas FROM ciclista",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) as ciclistas")->where("nomequipo = 'Banesto'"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['ciclistas'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) AS ciclistas FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta2(){
        $numero = Yii::$app->db
                ->createCommand('select count(*) as ciclistas from ciclista where nomequipo = "Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS ciclistas FROM ciclista WHERE nomequipo = "Banesto"',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['ciclistas'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) AS ciclistas FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta3a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("avg(edad) as edad"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) AS edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta3(){
        $numero = Yii::$app->db
                ->createCommand('select count(edad) as edad from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) AS edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) AS edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta4a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("avg(edad) as edad")->where("nomequipo = 'Banesto'"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) as edad FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta4(){
        $numero = Yii::$app->db
                ->createCommand('select count(edad) as edad from ciclista where nomequipo = "Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) as edad FROM ciclista WHERE nomequipo = "Banesto"',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) as edad FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta5a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nomequipo, avg(edad) as edad")->groupBy("nomequipo"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'edad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, AVG(edad) AS edad FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta5(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct nomequipo) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, AVG(edad) AS edad FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'edad'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, AVG(edad) AS edad FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta6a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nomequipo, count(*) as ciclistas")->groupBy("nomequipo"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'ciclistas'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta6(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct nomequipo) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'ciclistas'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta7a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("count(*) as puertos"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) AS puertos FROM puerto",
        ]);
    }
    
    public function actionConsulta7(){
        $numero = Yii::$app->db
                ->createCommand('select count(*) as puertos from puerto')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS puertos FROM puerto',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) AS puertos FROM puerto",
        ]);
    }
    
    public function actionConsulta8a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("count(*) as puertos")->where("altura > 1500"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) AS puertos FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsulta8(){
        $numero = Yii::$app->db
                ->createCommand('select count(*) as puertos from puerto where altura > 1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS puertos FROM puerto WHERE altura > 1500',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) AS puertos FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsulta9a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nomequipo, count(*) as ciclistas")->groupBy("nomequipo")->having("count(*) > 4"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'ciclistas'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    
    public function actionConsulta9(){
//        $numero = Yii::$app->db
//                ->createCommand('select nomequipo, count(*) as ciclistas from ciclista group by nomequipo having count(*) > 4')
//                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4',
//            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'ciclistas'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    
    public function actionConsulta10a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nomequipo, count(*) as ciclistas")->where("edad between 28 and 32")->groupBy("nomequipo")->having("count(*) > 4"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'ciclistas'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    
    public function actionConsulta10(){
//        $numero = Yii::$app->db
//                ->createCommand('select nomequipo, count(*) as ciclistas from ciclista where edad between 28 and 32 group by nomequipo having count(*) > 4')
//                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4',
//            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'ciclistas'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    
    public function actionConsulta11a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select("dorsal, count(dorsal) as etapas")->groupBy("dorsal"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'etapas'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta11(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) as etapas from etapa')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'etapas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta12a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select("dorsal, count(dorsal) as etapas")->groupBy("dorsal")->having("count(dorsal) > 1"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'etapas'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
        ]);
    }
    
    public function actionConsulta12(){
//        $numero = Yii::$app->db
//                ->createCommand('select count(distinct dorsal) as etapas from etapa having etapas > 1')
//                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1',
//            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'etapas'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
        ]);
    }
    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
