<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Ejercicios de consultas 2</h1>

        <p class="lead">Acceso a datos</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 1</h2>

                <p>Número de ciclistas que hay</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta1a">Active record</a>
                    <a class="btn btn-default" href="site/consulta1">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 2</h2>

                <p>Número de ciclistas que hay del equipo Banesto</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta2a">Active record</a>
                    <a class="btn btn-default" href="site/consulta2">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 3</h2>

                <p>Edad media de los ciclistas</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta3a">Active record</a>
                    <a class="btn btn-default" href="site/consulta3">DAO</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 4</h2>

                <p>La edad media de los del equipo Banesto</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta4a">Active record</a>
                    <a class="btn btn-default" href="site/consulta4">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 5</h2>

                <p>La edad media de los ciclistas por cada equipo</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta5a">Active record</a>
                    <a class="btn btn-default" href="site/consulta5">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 6</h2>

                <p>El número de ciclistas por equipo</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta6a">Active record</a>
                    <a class="btn btn-default" href="site/consulta6">DAO</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 7</h2>

                <p>El número total de puertos</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta7a">Active record</a>
                    <a class="btn btn-default" href="site/consulta7">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 8</h2>

                <p>El número total de puertos mayores de 1500</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta8a">Active record</a>
                    <a class="btn btn-default" href="site/consulta8">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 9</h2>

                <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta9a">Active record</a>
                    <a class="btn btn-default" href="site/consulta9">DAO</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 10</h2>

                <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta10a">Active record</a>
                    <a class="btn btn-default" href="site/consulta10">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 11</h2>

                <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta11a">Active record</a>
                    <a class="btn btn-default" href="site/consulta11">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 12</h2>

                <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta12a">Active record</a>
                    <a class="btn btn-default" href="site/consulta12">DAO</a>
                </p>
            </div>
        </div>
    </div>
</div>
